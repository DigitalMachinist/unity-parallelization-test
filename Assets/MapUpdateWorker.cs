﻿using System;
using System.Linq;
using UnityEngine;
using Axon.QuenchMap;
using Axon.Threading;

[RequireComponent( typeof( MapModel ) )]
public class MapUpdateWorker : UpdateWorker<MapUpdateWorker.Context>
{
    public struct Context
    {
        public PageModel[] Pages;
        public int FirstIndex;
        public int LastIndex;

        public Context( PageModel[] pages, int firstIndex, int lastIndex )
        {
            Pages = pages;
            FirstIndex = firstIndex;
            LastIndex = lastIndex;
        }
    }

    // Public/Inspector
    public int NumberOfBeginPhaseThreads  = 2;
    public int NumberOfMiddlePhaseThreads = 4;
    public int NumberOfEndPhaseThreads    = 1;
    
    // Lazy Properties
    MapModel __mapModel;
    public MapModel MapModel
    {
        get { return __mapModel ?? ( __mapModel = GetComponent<MapModel>() ); }
    }


    // Update algorithms
    void UpdateBegin( Context context )
    {
        Debug.Log( "UpdateBegin() called for pages " + context.FirstIndex + " through " + context.LastIndex + ".", this );
    }
    void UpdateMiddle( Context context )
    {
        Debug.Log( "UpdateMiddle() called for pages " + context.FirstIndex + " through " + context.LastIndex + ".", this );
        for ( var i = context.FirstIndex; i <= context.LastIndex; i++ )
        {
            foreach( var hex in context.Pages[ i ].Hexes )
            {
                hex.Count++;
            }
        }
    }
    void UpdateEnd( Context context )
    {
        Debug.Log( "UpdateEnd() called for pages " + context.FirstIndex + " through " + context.LastIndex + ".", this );
    }


    // Configure update tasks
    protected override void Awake()
    {
        base.Awake();

        UpdateTaskSets.Add( DividePagesAmongThreads( NumberOfBeginPhaseThreads, UpdateBegin ) );
        UpdateTaskSets.Add( DividePagesAmongThreads( NumberOfMiddlePhaseThreads, UpdateMiddle ) );
        UpdateTaskSets.Add( DividePagesAmongThreads( NumberOfEndPhaseThreads, UpdateEnd ) );
    }
    Task<Context>[] DividePagesAmongThreads( int numThreads, Action<Context> algorithm )
    {
        var pagesPerThread = MapModel.Data.Pages.Length / numThreads;
        return
            new Task<Context>[ numThreads ]
                .Select( ( task, index ) => {

                    var firstIndex = index * pagesPerThread + index;
                    var lastIndex = Mathf.Min(
                        ( index + 1 ) * pagesPerThread + index,
                        MapModel.Data.Pages.Length - 1
                    );

                    return new Task<Context>(
                        new Context( MapModel.Data.Pages, firstIndex, lastIndex ),
                        algorithm
                    );

                } )
                .ToArray();
    }
}
