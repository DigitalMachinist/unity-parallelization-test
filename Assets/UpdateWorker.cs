﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;


namespace Axon.Threading
{
    public class UpdateWorker<TContext> : MonoBehaviour
    {
        // Public/Inspector
        public bool UseThreadedUpdate = true;
        [Tooltip( "-1 == Timeout.Infinite" )]
        public int TaskSetTimeoutMS = -1;

        [Header( "Events" )]
        public UnityEvent BeganWorking;
        public UnityEvent FinishedWorking;

        // Private
        Coroutine        __coroutineHandle;
        Worker<TContext> __worker;
        WaitHandle       __workerFinishedHandle;

        // Properties
        public List<Task<TContext>[]> UpdateTaskSets { get; private set; }

        // Init
        protected virtual void Awake()
        {
            UpdateTaskSets = new List<Task<TContext>[]>();
            __worker = new Worker<TContext>( TaskSetTimeoutMS );
        }
        protected virtual void OnDestroy()
        {
            __worker.Stop();
        }

        // Update
        protected virtual void Update()
        {
            if ( UseThreadedUpdate )
            {
                // Start the threaded update going on the worker thread and then pass the WaitHandle to
                // a coroutine that will run at the end of the Unity thread's update for this frame. If 
                // the worker isn't finished by then, Unity will delay starting the next frame until 
                // the worker finishes its task or the handles WaitOne() times out.
                foreach ( var taskSet in UpdateTaskSets )
                {
                    BeginWork( __worker.EnqueueWork( taskSet ) );
                }
                __coroutineHandle = StartCoroutine( CoroutineWaitForWorkerFinished() );
            }
            else
            {
                // Just call the ThreadedUpdate function directly. This is less performant in most 
                // cases, but allows Unity's profiler to instrument the update code and deep profile 
                // the function calls.
                BeginWork( handle: null );
                foreach ( var taskSet in UpdateTaskSets )
                {
                    foreach ( var task in taskSet )
                    {
                        task.Execute( null );
                    }
                }
                FinishWork();
            }
        }
        void BeginWork( WaitHandle handle )
        {
            __workerFinishedHandle = handle;
            BeganWorking.Invoke();
        }
        void FinishWork()
        {
            if ( __coroutineHandle != null )
            {
                StopCoroutine( __coroutineHandle );
                __workerFinishedHandle = null;
            }
            FinishedWorking.Invoke();

            // Log any exceptions thrown by the worker's threaded update.
            foreach ( var exception in __worker.GetAndClearExceptions() )
            {
                Debug.LogException( exception, this );
            }
        }
        protected virtual IEnumerator CoroutineWaitForWorkerFinished()
        {
            if ( __workerFinishedHandle == null )
            {
                yield break;
            }
            yield return new WaitForEndOfFrame();
            __workerFinishedHandle.WaitOne();
            FinishWork();
        }
    }
}
