﻿using UnityEngine;
using Axon.QuenchMap;

[ExecuteInEditMode]
public class MapModel : MonoBehaviour
{
    [Header( "Initialization" )]
    public int StartPageRows = 4;
    public int StartPageCols = 4;
    public int StartHexRows = 8;
    public int StartHexCols = 8;

    [Header( "Model/State" )]
    public MapData Data;

	void Start()
    {
        // TODO Improve this to load actual level data.
        Data = new MapData( StartPageRows, StartPageCols, StartHexRows, StartHexCols );
	}
}
