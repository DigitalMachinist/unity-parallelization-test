﻿using System;

namespace Axon.QuenchMap
{
    [Serializable]
    public class PageModel
    {
        public int Col;
        public int Row;
        public int HexCols;
        public int HexRows;
        public HexModel[] Hexes;

        public PageModel( int rowIndex, int colIndex, int hexRows, int hexCols )
        {
            Col = colIndex;
            Row = rowIndex;
            HexCols = hexCols;
            HexRows = hexRows;
            Hexes = new HexModel[ hexRows * hexCols ];
            ForEachHex( ( index, row, col ) => {
                var hexRow = Row * HexRows + row;
                var hexCol = Col * HexCols + col;
                Hexes[ index ] = new HexModel( hexRow, hexCol );
            } );
        }

        public void ForEachHex( Action<int, int, int> action )
        {
            for ( var row = 0; row < HexRows; row++ )
            {
                for ( var col = 0; col < HexCols; col++ )
                {
                    var index = row * HexCols + col;
                    action( index, row, col );
                }
            }
        }
    }
}
