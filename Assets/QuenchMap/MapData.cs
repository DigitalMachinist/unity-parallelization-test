﻿using System;

namespace Axon.QuenchMap
{
    [Serializable]
    public class MapData
    {
        public int PageCols;
        public int PageRows;
        public PageModel[] Pages;

        public MapData( int pageRows, int pageCols, int hexRows, int hexCols )
        {
            PageCols = pageCols;
            PageRows = pageRows;
            Pages = new PageModel[ pageRows * pageCols ];
            ForEachPage( ( index, row, col ) => {
                Pages[ index ] = new PageModel( row, col, hexRows, hexCols );
            } );
        }

        public void ForEachPage( Action<int, int, int> action )
        {
            for ( var row = 0; row < PageRows; row++ )
            {
                for ( var col = 0; col < PageCols; col++ )
                {
                    var index = row * PageCols + col;
                    action( index, row, col );
                }
            }
        }
    }
}
