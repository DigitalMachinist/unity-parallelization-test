﻿using System;

namespace Axon.QuenchMap
{
    [Serializable]
    public class HexModel
    {
        public int Col;
        public int Row;
        public int Count;

        public HexModel( int rowIndex, int columnIndex )
        {
            Col = columnIndex;
            Row = rowIndex;
            Count = UnityEngine.Random.Range( 1, 10 );
        }
    }
}
