﻿using System;
using System.Linq;
using System.Threading;

namespace Axon.Threading
{
    public class Worker<TContext>
    {
        // Private
        ManualResetEvent __beganWorkHandle;
        ConcurrentQueue<Exception> __exceptionQueue;
        bool __isStopping;
        WaitHandle[] __taskDoneSignals;
        ConcurrentQueue<Task<TContext>[]> __taskQueue;
        int __taskSetTimeoutMS;
        Thread __thread;
        ManualResetEvent __waitHandle;

        // Properties
        public int TaskSetTimeoutMS
        {
            get { return __taskSetTimeoutMS; }
            set
            {
                __taskSetTimeoutMS =
                    ( value < 0 )
                        ? Timeout.Infinite
                        : value;
            }
        }

        // Computed Properties
        public WaitHandle BeganWorkSignal
        {
            get { return __beganWorkHandle; }
        }
        public WaitHandle WaitHandle
        {
            get { return __waitHandle; }
        }
        public bool IsWorking
        {
            get { return __beganWorkHandle.WaitOne( 0 ); }
        }


        // Constructor
        // -1 == Timeout.Infinite
        // Just cannot use it because unity doesn't know what to do when you use a constant
        // as a default parameter.
        public Worker( int timeoutMS = -1 )
        {
            TaskSetTimeoutMS = timeoutMS;
            __beganWorkHandle = new ManualResetEvent( false );
            __exceptionQueue = new ConcurrentQueue<Exception>();
            __taskQueue = new ConcurrentQueue<Task<TContext>[]>();
            __waitHandle = new ManualResetEvent( false );
            __thread = new Thread( ThreadProc );
            __thread.Start();
        }


        // Thread Worker
        void ThreadProc()
        {
            while ( !__isStopping )
            {
                // Wait for the update begin signal to be triggered before entering into the
                // update process below.
                __beganWorkHandle.WaitOne();

                try
                {
                    // Continue processing task sets until the queue is empty, then report signal 
                    // completion to the caller of 
                    while ( __taskQueue.Count > 0 )
                    {
                        // Reset each task now that they have all completed for this frame and then
                        // launch each task as a thread in the ThreadPool. We need to call Reset() 
                        // on each task so they are set back to their in-work state before the 
                        // WaitHandle.WaitAll() call below tests them for completion!
                        var tasks = __taskQueue.Dequeue();
                        foreach ( var task in tasks )
                        {
                            task.Reset();
                            ThreadPool.QueueUserWorkItem( task.Execute );
                        }

                        // Extract an array of all done signals associated with each of the tasks 
                        // to pass into WaitHandle.WaitAll() later on.
                        __taskDoneSignals =
                            tasks
                                .Select( task => task.WaitHandle )
                                .ToArray();

                        // Begin waiting for all tasks to complete on this thread.
                        WaitHandle.WaitAll( __taskDoneSignals, __taskSetTimeoutMS );

                        // If any tasks encountered exceptions, push them into the exception queue.
                        foreach ( var task in tasks.Where( task => task.Exception != null ) )
                        {
                            __exceptionQueue.Enqueue( task.Exception );
                        }
                    }
                }
                catch ( ThreadInterruptedException )
                {
                    Stop();
                }
                catch ( Exception e )
                {
                    // Push any exceptions that have been thrown to a queue so we can see them.
                    __exceptionQueue.Enqueue( e );
                }
                finally
                {
                    // Reset the begin signal to indicate that the thread has finished this update
                    // and is ready for the next one.
                    __beganWorkHandle.Reset();

                    // Set the signal for the AutoResetEvent to signal any other threads waiting on
                    // this update to complete to move on.
                    __waitHandle.Set();
                }
            }
        }


        #region Helpers

        public WaitHandle EnqueueWork( Task<TContext> task )
        {
            return EnqueueWork( new Task<TContext>[] { task } );
        }

        public WaitHandle EnqueueWork( Task<TContext>[] tasks )
        {
            // Reset the work finished signal before allowing ThreadProc() to run.
            __waitHandle.Reset();

            // Push the new tasks onto the task queue to be handled in parallel by ThreadProc().
            // If the queue was previously empty, then the begin work signal should be set to start
            // ThreadProc() up again.
            __taskQueue.Enqueue( tasks );
            if ( __taskQueue.Count == 1 )
            {
                __beganWorkHandle.Set();
            }

            // Return the wait handle to the caller as a convenience to save them having to look up
            // the WaitHandle member separately.
            return __waitHandle;
        }

        public Exception[] GetAndClearExceptions()
        {
            Exception[] exceptions = __exceptionQueue.CopyToArray();
            __exceptionQueue.Clear();
            return exceptions;
        }

        public void Stop()
        {
            __isStopping = true;
        }

        #endregion
    }
}
