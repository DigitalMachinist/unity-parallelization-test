﻿using System;
using System.Threading;

namespace Axon.Threading
{
    public class Task<TContext>
    {
        Action<TContext> __algorithm;
        TContext         __context;
        ManualResetEvent __waitHandle;
        Exception        __exception;


        public WaitHandle WaitHandle
        {
            get { return __waitHandle; }
        }
        public Exception Exception
        {
            get { return __exception; }
        }
        public bool IsDone
        {
            get { return __waitHandle.WaitOne( 0 ); }
        }
        public bool IsWorking
        {
            get { return !IsDone; }
        }


        public Task( TContext context, Action<TContext> algorithm )
        {
            __waitHandle = new ManualResetEvent( false );
            Init( context, algorithm );
        }
        public void Init( TContext context, Action<TContext> algorithm )
        {
            if ( algorithm == null )
            {
                throw new ArgumentNullException( "Algorithm must be non-null!" );
            }
            __algorithm = algorithm;
            __context = context;
            Reset();
        }


        public void Execute( object unused )
        {
            try
            {
                __algorithm( __context );
            }
            catch ( Exception e )
            {
                __exception = e;
            }
            __waitHandle.Set();
        }

        public void Reset()
        {
            __waitHandle.Reset();
            __exception = null;
        }
    }
}
